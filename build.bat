@echo off
mkdir build
del /s "build\build.out"
del /s "build\build.err"
del /s "build\build.html"
gradlew build --profile > build/build.out 2> build/build.err
start build\build.html