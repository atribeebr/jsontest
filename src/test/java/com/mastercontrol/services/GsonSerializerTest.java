package com.mastercontrol.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 11/28/2016.
 */
public class GsonSerializerTest {
    private TestHelper objectMaker = new TestHelper();

    @Test
    public void serializeWithGson() throws Exception {
        assertEquals(GsonSerializer.serializeWithGson(objectMaker.movieObject()),objectMaker.gsonMovieJson());
    }

    @Test
    public void deserializeWithGson() {
        assertEquals(GsonSerializer.deserializeWithGson(objectMaker.gsonMovieJson()), objectMaker.movieObject());
    }

}