package com.mastercontrol.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 11/29/2016.
 */
public class JacksonSerializerTest {
    private TestHelper objectMaker = new TestHelper();

    @Test
    public void serializeWithGson() throws Exception {
        assertEquals(JacksonSerializer.serializeWithGson(objectMaker.movieObject()),objectMaker.jacksonMovieJson());
    }

    @Test
    public void deserializeWithGson() throws Exception {
        assertEquals(JacksonSerializer.deserializeWithGson(objectMaker.jacksonMovieJson()), objectMaker.movieObject());
    }

}