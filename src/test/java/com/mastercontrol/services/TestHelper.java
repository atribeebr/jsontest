package com.mastercontrol.services;

import com.mastercontrol.models.Actor;
import com.mastercontrol.models.Movie;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Holds the strings and objects to convert and test against.
 */
public class TestHelper {

    /**
     * Creates a movie json string with a written out date.
     * @return json string with date for Gson
     */
    public String gsonMovieJson() {
        return "{\"imdbId\":\"tt0472043\",\"director\":\"Mel Gibson\",\"actors\":[{\"imdbId\":\"nm2199632\",\"dateOfBirth\":\"Sep 21, 1982 12:00:00 AM\",\"filmography\":[\"Apocalypto\",\"Beatdown\",\"Wind Walkers\"]}]}";
    }

    /**
     * Creates a movie json string with a epoch format date.
     * @return json string with date for Jackson
     */
    public String jacksonMovieJson() {
        return "{\"imdbId\":\"tt0472043\",\"director\":\"Mel Gibson\",\"actors\":[{\"imdbId\":\"nm2199632\",\"dateOfBirth\":401436000000,\"filmography\":[\"Apocalypto\",\"Beatdown\",\"Wind Walkers\"]}]}";
    }

    /**
     * Creates a movie object.
     * @return movie object.
     */
    public Movie movieObject() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date birthDate = null;
        try {
            birthDate = sdf.parse("21-09-1982");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Actor rudyYoungblood = new Actor(
                "nm2199632",
                birthDate,
                Arrays.asList("Apocalypto",
                        "Beatdown", "Wind Walkers")
        );

        return new Movie(
                "tt0472043",
                "Mel Gibson",
                Arrays.asList(rudyYoungblood));
    }
}
