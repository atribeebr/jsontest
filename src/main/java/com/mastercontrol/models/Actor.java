package com.mastercontrol.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Simple actor object.
 */
@Data
@AllArgsConstructor
public class Actor {
    private String imdbId;
    private Date dateOfBirth;
    private List<String> filmography;
}
