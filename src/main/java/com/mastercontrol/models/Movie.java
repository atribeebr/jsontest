package com.mastercontrol.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Simple movie object.
 */
@Data
@AllArgsConstructor
public class Movie {
    private String imdbId;
    private String director;
    private List<Actor> actors;


}
