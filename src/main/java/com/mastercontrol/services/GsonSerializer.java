package com.mastercontrol.services;

import com.google.gson.Gson;
import com.mastercontrol.models.Movie;

/**
 * Example of serializing and deserializing with Gson.
 */
public final class GsonSerializer {

    private GsonSerializer() {
    }

    private static Gson gson = new Gson();

    /**
     * Tests Gson JSON serialization.
     * @param movie a movie object to serialize
     * @return JSON String
     */
    public static String serializeWithGson(final Movie movie) {
        return gson.toJson(movie);
    }

    /**
     * Parse a json string into an object.
     * @param json input json string.
     * @return Movie object
     */
    public static Movie deserializeWithGson(final String json) {
        return gson.fromJson(json, Movie.class);
    }
}
