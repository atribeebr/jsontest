package com.mastercontrol.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mastercontrol.models.Movie;

import java.io.IOException;

/**
 * Example of serializing and deserializing with Jackson.
 */
public final class JacksonSerializer {

    private JacksonSerializer() {
    }

    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * Tests Gson JSON serialization.
     * @param movie a movie object to serialize
     * @return JSON String
     */
    public static String serializeWithGson(final Movie movie) {

        String serializedMovie = null;
        try {
            serializedMovie = mapper.writeValueAsString(movie);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return serializedMovie;
    }

    /**
     * Parse a json string into an object.
     * @param json input json string.
     * @return Movie object
     */
    public static Movie deserializeWithGson(final String json) {

        Gson gson = new Gson();

        Movie movie = null;
        try {
            movie = mapper.readValue(json, Movie.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movie;
    }
}
